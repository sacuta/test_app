class Parser
  def self.all
    ('A'..'Z').each do |char|
      parse_and_save(char)
    end
  end

  def self.parse_and_save(_name, _country='RUS')
    # example: dph_RegistryContent_SearchResults_ctl08_registryListItem -> dph_RegistryContent_SearchResults_ctl08_nameLabel1
    url = "https://certification.pmi.org/registry.aspx"
    form_id_selector = '_Form'
    lastname_id_selector = 'dph_RegistryContent_tbSearch'
    country_id_selector = 'dph_RegistryContent_wcountry'

    agent = Mechanize.new
    login_form = agent.get(url).form(form_id_selector)
    login_form.field_with(id: lastname_id_selector).value = _name
    login_form.field_with(id: country_id_selector).value = _country
    page = login_form.submit

    rows = page.search('#article table tr')
    rows.shift

    data = []

    rows.each do |row|
      tr_id = row.attributes['id'].value
      person = {}
      name, city, country, credential, earned, status = search_items(row, tr_id)
      person[:name] = name.present? ? name : data.last[:name]
      person[:city] = city.present? ? city : data.last[:city]
      person[:country] = country.present? ? country : data.last[:country]
      person[:credential] = credential.present? ? credential : data.last[:credential]
      person[:earned] = earned.present? ? earned : data.last[:earned]
      person[:status] = status.present? ? status : data.last[:status]
      data << person
    end

    Person.save_all(data)
  end

  def self.search_items(row, id)
    labels = ['nameLabel', 'cityStateLabel', 'countryLabel', 'certTypeLabel', 'certDateLabel', 'certStatusLabel']
    items = []
    labels.each do |label|
      items << row.search(get_id_selector(id, label)).text
    end
    items
  end

  def self.get_id_selector(id, label)
    tr_label = 'registryListItem'
    '#' + id.gsub(tr_label, label + '1')
  end

end