class Person < ApplicationRecord
  def self.save_all(items)
    items.each { |item| Person.new(item).save! }
  end
end
