json.extract! person, :id, :name, :city, :country, :credential, :earned, :status, :created_at, :updated_at
json.url person_url(person, format: :json)
