Rails.application.routes.draw do
  resources :people
  get 'hello/world'
  post 'people/get_data'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
