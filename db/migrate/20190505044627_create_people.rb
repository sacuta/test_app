class CreatePeople < ActiveRecord::Migration[5.2]
  def change
    create_table :people do |t|
      t.string :name
      t.string :city
      t.string :country
      t.string :credential
      t.date :earned
      t.string :status

      t.timestamps
    end
  end
end
