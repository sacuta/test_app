require 'rails_helper'

RSpec.describe Parser, type: :model do
  it "should be equal" do
    selector = Parser.get_id_selector('MY_ID_FOR_TEST_registryListItem', 'WITH_LABEL')
    expect(selector).to eq('#MY_ID_FOR_TEST_WITH_LABEL1')
  end
end
