require 'rails_helper'

RSpec.describe Person, type: :model do
  it "should be equal" do
    items = [
        {name: 'Name1', city: 'Samara', country: 'Russia', credential: 'AAA', earned: '28 Apr 2017', status: 'Active'},
        {name: 'Name1', city: 'Samara', country: 'Russia', credential: 'AAA2', earned: '28 Apr 2018', status: 'Active'},
        {name: 'Name2', city: 'Moscow', country: 'Russia', credential: 'AAA', earned: '21 Apr 2017', status: 'Active'},
    ]
    Person.save_all(items)
    expect(Person.find_by_credential('AAA2').name).to eq('Name1')
  end
end
