require 'rails_helper'

RSpec.describe "people/edit", type: :view do
  before(:each) do
    @person = assign(:person, Person.create!(
      :name => "MyString",
      :city => "MyString",
      :country => "MyString",
      :credential => "MyString",
      :status => "MyString"
    ))
  end

  it "renders the edit person form" do
    render

    assert_select "form[action=?][method=?]", person_path(@person), "post" do

      assert_select "input[name=?]", "person[name]"

      assert_select "input[name=?]", "person[city]"

      assert_select "input[name=?]", "person[country]"

      assert_select "input[name=?]", "person[credential]"

      assert_select "input[name=?]", "person[status]"
    end
  end
end
