require 'rails_helper'

RSpec.describe "people/index", type: :view do
  before(:each) do
    assign(:people, [
      Person.create!(
        :name => "Name",
        :city => "City",
        :country => "Country",
        :credential => "Credential",
        :status => "Status"
      ),
      Person.create!(
        :name => "Name",
        :city => "City",
        :country => "Country",
        :credential => "Credential",
        :status => "Status"
      )
    ])
  end

  it "renders a list of people" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "City".to_s, :count => 2
    assert_select "tr>td", :text => "Country".to_s, :count => 2
    assert_select "tr>td", :text => "Credential".to_s, :count => 2
    assert_select "tr>td", :text => "Status".to_s, :count => 2
  end
end
