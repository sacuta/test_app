require 'rails_helper'

RSpec.describe "people/new", type: :view do
  before(:each) do
    assign(:person, Person.new(
      :name => "MyString",
      :city => "MyString",
      :country => "MyString",
      :credential => "MyString",
      :status => "MyString"
    ))
  end

  it "renders new person form" do
    render

    assert_select "form[action=?][method=?]", people_path, "post" do

      assert_select "input[name=?]", "person[name]"

      assert_select "input[name=?]", "person[city]"

      assert_select "input[name=?]", "person[country]"

      assert_select "input[name=?]", "person[credential]"

      assert_select "input[name=?]", "person[status]"
    end
  end
end
